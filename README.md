# Denoise low dose cardiac CT's with ED-CNN architecture
This file contains instructions for testing and training the ED-CNN model, and explanations on the different blocks of the code.

## Model details
### Network architecture
The model is based on EDCNN network from https://arxiv.org/pdf/2011.00139.pdf .
The basic architecture is a convolutional neural network with 8 couples of 1X1 and 3X3 convolution layers, each with LeakyReLU activation.
To avoid loss of information, the input is concatenated with each layer output, a technique called skip connections. Before final activation, the noisy input image is added to the last layers output. The input to the CNN is the output of a sobel based edge enhancement model, concatenated with the noisy input image.
![im](ims/EDCNN.PNG?raw=true "EDCNN architecture")

The sobel based edge enhancement model is fixed sobel edge detection filters multiplied by a learnable parameter, that are convolved with the noisy input image.  
![im2](ims/sobel.PNG?raw=true "EDCNN architecture")

### Loss
The ground truth for the training process is composed of ASIR algorithm output, and of NLM algorithm, that is performed on ASIR output. 
The loss that is minimized is based on 2 losses:
1) MSE loss - comparing the output of the network to ASIR+NLM pixel-wise. 
2) perceptual loss- passing both the networks output and ground truth through resnet50 model, and comparing (MSE) the outputs for each one of the four main blocks of resnet50.
![im3](ims/perceptual_loss.PNG?raw=true "EDCNN architecture")

Perceptual loss helps keeping features of the image.
The two ground truths (ASIR and AIR+NLM) were compared to the networks output, where each of this loses was multiplied by 0.1, to avoid a large shift in mean color from original image.
   The reason for using the two ground truths is that ASIR helps to better preserve features, but when used alone it also preserves some noise.
### Double input
A modification that was made is concatenating a second input, together with the noisy image and sobel output.
This second input is the same noisy image that is blurred pre reconstruction. 
This double input method generates better smoothed images, without any loss of details.
## Git files
### Code blocks
* **prep.py**- Used to transform the dicom data into normalized np data, so that it can be processed by the model
* **loader_data_NLM.py**- Used to prepare a dataloader that easily fids data into the testing/training process, in the form of mini batches of data each iteration. If needed, in this file the data can be augmented before training (noise, rotate, flip or zoom)
* **src/model.py**- Architecture of ED-CNN 
* **src/loss.py**- Loss function of the training process. Introduces RESNET model used for perceptual loss. 
* **solver.py**- Contains the main operations of the model- include training and testing processes. 
* **main.py**- Used to run the whole train/test process. prep is not included in main and should be used separately.
* **config.py**- Contains all configuration data and variables.
### Other files
* **linux_env.yml**- Settings file for working environment for linux operating system.
* **windows_env.yml**- Settings file for working environment for windows operating system.
* **double_persep_weights_230000iter.ckpt**- Pre trained weights of double perceptual loss model, to load in test.
  * **double_persep_double_input_weights_235000iter**- Pre trained weights of double perceptual loss and double input model, to load in test.
* **deployment**- containing a machine deployment version of the code, with C++ interface.
## Run the code
### Create environment:
Two requirements files are added- one for linux and one for windows. Assuming windows operating system: 
Use windows_env.yml file to set the requirements in your environment:
```linux
conda env create --file windows_env.yml
```
After installation, activate environment with:
```linux
conda activate torch
```
### Prepare data
The model works on pre saved numpy images. prep.py helps to transform dicom folders into numpy images.
```linux
python prep.py --datapath "your_saved_data_path" --save_path "path_to_save_numpy_arrays" --single_case "name of single case" 
```
#### Configuration options for prep:
* **data_path**- The data path must be a folder containing all cases. Every case must have sub folders containing relevant dicom slices. ASIR folder must contain the word ASIR in its name, NLM folders must contain NLM in its name, original folders must not contain NLM or ASIR in its name.

* **np_save_path**- Just specify a path at which created np arrays are saved. All slices are saved together in the same folder, even if there is more than one patient. For this reason every CT patient folder should be denoised separately. 
  
* **single_case**- To prepare trainset with multiple cases, should be None, as Default. For testing on a single case, should be the case name, i.e. the relevant folder name that contains the dicom folder to be tested.

* **create_NLM**-  If there is no NLM in the data path, one can be generated before transforming to numpy by setting ```linux --create_NLM True ``` 
**<u>example</u>**: To test */arineta/data/reordered_poc_data/Friendship_ST_000418/original/*  
  set ``` --single_case "Friendship_ST_000418" ``` .
#### Extra options in prep:
* If data has only original and ASIR folders, NLM folders can be created, type: ```--create_NLM True```.

### Testing module
To denoise the np CT created with prep, using the model, type:
```linux
python main.py --saved_path "your_saved_np_data_path" --save_path "path_to_save_predictions" --dicom_path "path_to_original_dicom" --test_patient "patient_id" 
```
#### Configuration options for testing:
* **saved_path**- The data path where np arrays are saved (same as specified in prep save_path)
* **save_path**- A path at which predictions are saved (both np and dicom versions).
* **dicom_path**- A path to the original dicom that is being denoised. The path should be directly to the folder containing the slices.
* **test_patient**- Choose the name of the folder that will be created in save_path at which the dicom slices are saved.
#### Extra options in testing:
* **transfer_weights**- To change the tested weights specify the path to the other weights with ```--transfer_weights "weights_path"```.
* **apply_nlm**- To apply NLM before saving prediction, type ```--apply_nlm True```. default is False. warning- may take long time at current version.
* **series_description**- Description inside the predicted dicom can be specified using ```--series_description "description"```.

### Training module
There is also an option to train new weights. Prep can generate multiple np CTs to train on if datapath folder provided contains multiple cases. To perform training:
```linux
python main.py --mode 'train' --saved_path "your_saved_np_data_path" --save_path "path_to_save_weights_checkpoints"  
```
#### Configuration options for training:
* **mode**- Define as 'train'. Default of mode is 'test'.  
* **saved_path**- The data path where np arrays are saved (same as specified in prep save_path). 
* **save_path**- A path at which weights and training loss graph are saved.

#### Extra options in train:
* **test_patient**- If there is a case that is processed by prep, but should not be part of training, specify its name as it is in its original folder: ```--test_patient "patient_id_to_ignore_in_train"```.
* **init_with_transfer**- To initialize weights before training with pretrained weights, type: ``` --init_with_transfer True --transfer_weights "weights_path"```.
* **device**- To change process device (gpu/cpu), type: ``` --device "device_name"```. Default is "cuda:0".
* **num_workers**- To change the number of parallel processors working , type: ``` --num_workers <int>```. Default is 6.


### Frontend model with autogenerated paths
The main model is initialized such that it performs prep within the main model, and auto generates the saving paths. It also chooses by itself the dicom path, based on the "data_path" variable.
To use this frontend model, just change the data_path parameter to the folder containing the case you would like to test. 
Notice that the folder structure should be "folder/cases_names/orig_asir_and_nlm(or just orig)/dicom_slices".
If there is more than one case in "folder", please change the parameter "single_case" to the case name.
The outputs of the model would be saved at a folder at the same location as the data_path folder, with the same name+"Outputs".
This method is for testing only. if you would like to disable it you can change the bools "generate_path" and "auto_prep" to False.

### double input option
In order to train/test double input model, the parameter "double input" in config file should be set to True, and a folder of smoothed input should also be located where orig, and asir are. Make sure that this folder has the word "smooth" in its name, so the prep.py would identify it.
The previously trained weights are also uploaded to this git, to test them make sure to set ``` --transfer_weights  "double_persep_double_input_weights_235000iter.ckpt"``` whether in cmd or in config file.

***
** **notice!** All dicom slices processed should be named with the format of one character and then the slice number. For example: "I00006.dcm" is a good slice name whereas "DICOM00006.dcm" is not a good name.  
** For more information on parameters go to config.py 