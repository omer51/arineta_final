#include "CRSIPProcessor.h"
#include "CRSIWorker.h"
#include <sstream>

// Using dynamic-link version of the library
#include <zmq.hpp>

#include <iostream>
#include <array>


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <processthreadsapi.h>
#endif

// external functions

static bool WriteToBuffer(std::vector<char>& buf, char i, int& index);

static bool WriteToBuffer(std::vector<char>& buf, int i, int& index);

static bool WriteToBuffer(std::vector<char>& buf, int const* pf, int count, int& index);

static bool ReadFromBuffer(const zmq::message_t& buf, char& i, int& index);

static bool ReadFromBuffer(const zmq::message_t& msg, std::vector<char>& new_buf, int im_size, int& index);

static void ServerSend(std::vector<char>& buf, zmq::message_t& reply);

static void ServerSendTestBuffer(int bufferSize);


// define messages type
enum class MessageType : char
{
    EXIT  = -10,
    NOK   =  50,
    OK    =   0,
    DENOISE =   1,
    TEST  = 100,
};

static zmq::context_t zmq_rsip_context(1);
static zmq::socket_t zmq_rsip_socket(zmq_rsip_context, ZMQ_REQ);

namespace rsip
{

// RSIPDenoiser Constructor, destructor
    RSIPDenoiser::RSIPDenoiser(
        const std::string& childProcessCommandLine,
        const std::string& childProcessPortString)
        : _childProcessPortString(childProcessPortString)
    {
        // CRSIWorker::Spawn(childProcessCommandLine);
        std::cerr << "[Client] Starting processing server:" << std::endl << childProcessCommandLine << std::endl;

        // This is blocking:
        // std::system(childProcessCommandLine.c_str());
        STARTUPINFOA si;
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        PROCESS_INFORMATION pi;
        ZeroMemory(&pi, sizeof(pi));
        if (childProcessCommandLine.size() > 1000)
            throw false;
        std::array<char, 1000> commandLine;
        std::copy(childProcessCommandLine.begin(), childProcessCommandLine.end(), commandLine.data());
        commandLine[childProcessCommandLine.size()] = 0;
        int proc = CreateProcessA(
            nullptr,
            commandLine.data(),
            nullptr,
            nullptr,
            false,
            NORMAL_PRIORITY_CLASS,
            nullptr,
            nullptr,
            &si,
            &pi
        );
        if (proc) 
        {
            std::cerr << "[Client] RSIPDenoiser() launched command :" << (childProcessCommandLine + " &").c_str() << std::endl;
        }
        else 
        {
            std::cerr << "[Client] RSIPDenoiser() fail to launch command :" << (childProcessCommandLine + " &").c_str() << std::endl;
            valid = false;
        }

    zmq_rsip_socket = zmq::socket_t(zmq_rsip_context, ZMQ_REQ);
    zmq_rsip_socket.setsockopt(ZMQ_LINGER, 0);
    zmq_rsip_socket.connect(_childProcessPortString);

    if (false)
        ServerSendTestBuffer(100);  // Can be used to debug communication problems
}


    RSIPDenoiser::~RSIPDenoiser()
{
        if (valid)
        {
            // Tell server to exit
            std::cout << "[Client] Sending server EXIT command" << std::endl;
            zmq::message_t request(1);
            *((char*)request.data()) = (char)MessageType::EXIT;
            zmq_rsip_socket.send(request);

            zmq::message_t reply;
            zmq_rsip_socket.recv(&reply);
            std::cout << "[Client] Reply received - "
                << "length: " << reply.size()
                << " Content: " << *((char*)reply.data()) << std::endl;
            if (reply.size() != 1 || *((char*)reply.data()) != (char)MessageType::OK)
            {
                std::cout << "[Client] Invalid reply received from server" << std::endl;

            }
            std::cout << "[Client] Server confirmed EXIT command" << std::endl;
        }
        else 
        {
            std::cout << "[Client] failed connection, denoising failed!" << std::endl;
            
        }
    
}


    bool RSIPDenoiser::isReady(std::string& Err)
    {
        if (!valid) 
        {
            Err = "Failed to launch command, childprocess was not initiated";
            return false;
        }
        // Validate connection with server
        int recv_timeout = 10000;
        zmq::message_t request(1);
        *((char*)request.data()) = (char)MessageType::OK;
        zmq_rsip_socket.send(request, ZMQ_NOBLOCK);
        zmq::message_t reply;

        if (recv_timeout > 0)
        {
            zmq::pollitem_t pollitem;
            pollitem.socket = zmq_rsip_socket;
            pollitem.events = ZMQ_POLLIN | ZMQ_POLLERR;
            int poll_ret = zmq::poll(&pollitem, 1, recv_timeout);
            if (poll_ret == 0) {
                std::cerr << "[Client] Receive timed out after " << recv_timeout << " ms." << std::endl;
                Err = "failed to establish connection with python file";
                valid = false;

                return false;
            }
            else {
                std::cerr << "[Client] valid connection! "  << std::endl;
                zmq_rsip_socket.recv(&reply);
                if (reply.size() != 1 || *((char*)reply.data()) != (char)MessageType::OK)
                {
                    Err = "Bad reply received at connection test";
                    std::cerr << "[Client] Bad reply received" << std::endl;
                    return(false);
                }
                else
                    std::cout << "[Client] Handshake confirmed" << std::endl;
            }
        }
        return true;
    }


bool RSIPDenoiser::process_DENOISE(
    int const* const InBuffer, int nRows, int nCols, int nSlices, std::vector<char> &Out, std::string& sError)
{
    // Prepare buffer: nRows, ,nCols, nSlices, InBuffer (16 bit ints)
    int bufferSize = 1 + 2 * 3 + 2 * (nRows * nCols * nSlices);
    std::vector<char> buf(bufferSize);
    int index = 0;
    WriteToBuffer(buf, (char)MessageType::DENOISE, index);
    WriteToBuffer(buf, nRows, index);
    WriteToBuffer(buf, nCols, index);
    WriteToBuffer(buf, nSlices, index);
    WriteToBuffer(buf, InBuffer, nRows * nCols * nSlices, index);

    // Send message and receive reply
    zmq::message_t reply;
    ServerSend(buf, reply);
    index = 0;
    char messageType;
    ReadFromBuffer(reply, messageType, index);
    if (MessageType(messageType) != MessageType::OK)
    {
        // get error length
        std::vector<char> err_size_buf(6);
        ReadFromBuffer(reply, err_size_buf, 6, index);
        std::string s_err_len(err_size_buf.begin(), err_size_buf.end());
        int err_len;
        std::stringstream ss;
        ss << s_err_len;
        ss >> err_len;

        // get error
        std::vector<char> err_buf(err_len);
        ReadFromBuffer(reply, err_buf, err_len, index);
        std::string s_err(err_buf.begin(), err_buf.end());
        //std::cout << "err is: " << "\n" << s_err << "what" << std::endl;
        sError = ("Python inner error:\n"+s_err).c_str();
        return false;
    }
    int new_bufferSize = 2 * (nRows * nCols * nSlices);
    std::vector<char> new_buf(new_bufferSize);
    ReadFromBuffer(reply, Out, new_bufferSize, index);
    
    return true;
}
}


// Local functions
static bool WriteToBuffer(std::vector<char>& buf, char i, int& index)
{
    int size = 1;
    if (size_t(index + size) > buf.size())
        throw false;
    memcpy(buf.data() + index, (void*)&i, size);
    index += size;
    return true;
}

static bool WriteToBuffer(std::vector<char>& buf, int i, int& index)
{
    int size = 2;
    if (size_t(index + size) > buf.size())
    {
        std::cerr << "[Client] Buffer overwrite detected: "
            << "buffer size is " << buf.size()
            << " data size is " << size
            << " and index is " << index << std::endl;
        throw false;
    }
    memcpy(buf.data() + index, (void*)&i, size);
    index += size;
    return true;
}

static bool WriteToBuffer(std::vector<char>& buf, int const* pf, int count, int& index)
{
    int size = count * 2;
    if (size_t(index + size) > buf.size())
        throw false;
    memcpy(buf.data() + index, (void*)pf, size);
    index += size;
    return true;
}

static bool ReadFromBuffer(const zmq::message_t& buf, char& i, int& index)
{
    int size = 1;
    if (size_t(index + size) > buf.size())
        throw false;
    memcpy((void*)&i, (char *)(buf.data()) + index, size);
    index += size;
    return true;
}

static bool ReadFromBuffer(const zmq::message_t& buf, std::vector<char>& new_buf, int im_size, int& index)
{
    int size = im_size;
    if (size_t(index + size) > buf.size())
        throw false;
    memcpy(new_buf.data(), (char *)(buf.data()) + index, size);
    index += size;
    return true;
}

// Send buffer and receive reply
static void ServerSend(std::vector<char>& buf, zmq::message_t& reply)
{
    zmq::message_t request(buf.size());
    std::memcpy(request.data(), buf.data(), buf.size());
    zmq_rsip_socket.send(request);

    //  Wait for reply from client
    zmq_rsip_socket.recv(&reply);
}



static void ServerSendTestBuffer(int bufferSize)
{
    std::vector<char> buf(bufferSize);
    int index = 0;
    WriteToBuffer(buf, (char)MessageType::TEST, index);
    WriteToBuffer(buf, bufferSize, index);
    index = bufferSize - 2;  // Write at end of buffer
    WriteToBuffer(buf, bufferSize, index);
    zmq::message_t reply;
    ServerSend(buf, reply);
}

