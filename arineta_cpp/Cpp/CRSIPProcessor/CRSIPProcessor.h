#include <string>
#include <vector>

namespace rsip

{


/*!
 * Instance of object use for processing image
 */
class RSIPDenoiser
{
public:
    // Constructor, destructor
    RSIPDenoiser(
        const std::string& childProcessCommandLine,
        const std::string& childProcessPortString);
    ~RSIPDenoiser();


    bool isReady(std::string& Err);

    /*!
     * Denoise slices of CT array
     * \param pInBuffer : the internal buffer storing the B mode image
     * \param nRows : size of image
     * \param nCols : size of image
     * \param nSlices : number of images
     * \param Out : an array to hold output string of bytes representing denoised CT
     * \param sError : a string holding error if process failed
     * \return true if successful, false otherwise
     */
    bool process_DENOISE(
        int const* const InBuffer, int nRows, int nCols, int nSlices, std::vector<char> &Out, std::string& sError);


private:
    const std::string _childProcessPortString;
    bool valid = true;
};

}

