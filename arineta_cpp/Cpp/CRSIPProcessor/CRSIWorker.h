#pragma once

#include <cstdlib>
#include <iostream>


class CRSIWorker
{
public:
    static void Spawn(const std::string& commandLine);
};


void CRSIWorker::Spawn(const std::string& commandLine)
{
    bool result  = std::system(commandLine.c_str());
    if (!result)
    {
        std::cerr << "CRSIWorker::Spawn fail to launch command :" << commandLine << std::endl;
    }
}
