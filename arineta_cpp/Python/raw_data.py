from glob import glob as glob
import numpy as np
import os
import config
import matplotlib.pyplot as plt


bulet = 3


if bulet == 0:
    input_path = sorted(glob(os.path.join("/Users/omerc/Downloads/test_tests_outputs/un_normalized_npy", '*_original.npy')))
    dir = "/Users/omerc/Downloads/test_tests_outputs/test_tests_raw_data/"
    if not os.path.exists(dir):
        os.makedirs(dir)
    for i, name in enumerate(input_path):
        slice = np.load(name)
        print(slice.shape)
        #plt.imshow(slice, cmap='gray', vmin=0, vmax=0.5)
        #plt.show()
        file_name = os.path.basename(name)
        file_name = file_name.replace(".npy", ".rawData")

        slice.tofile(dir+file_name)

elif bulet == 1:
    input_path = sorted(glob(os.path.join("/Users/omerc/Downloads/test_tests_outputs/test_tests_raw_data/", '*_original.rawData')))
    print(len(input_path))
    dir = "/Users/omerc/Downloads/test_tests_outputs/test_tests_np_from_raw_data/"
    if not os.path.exists(dir):
        os.makedirs(dir)
    for i, name in enumerate(input_path):
        slice = np.fromfile(name, dtype="int16").reshape((512,512))
        file_name = os.path.basename(name)
        file_name = file_name.replace(".rawData", ".npy")

        np.save(dir+file_name, slice)

elif bulet == 2:
    a = np.load("/Users/omerc/Downloads/test_tests_outputs/test_tests_np_from_raw_data/Exam1_000100_original.npy")
    b = np.load("/Users/omerc/Downloads/test_tests_outputs/un_normalized_npy/Exam1_000100_original.npy")
    a = a.reshape((512,512))
    plt.imshow(a, cmap='gray', vmin=-300, vmax=500)
    plt.show()
    plt.imshow(b, cmap='gray', vmin=-300, vmax=500)
    plt.show()

if bulet == 3:
    name = "/Users/omerc/Documents/cpp_arineta/supersonic-us-breast-lesion-segmentation/Cpp/CRSIPProcessor/CRSIPProcessorRunner/data.rawData"
    a = np.fromfile(name, dtype="int16").reshape((512, 512))
    plt.imshow(a, cmap='gray', vmin=-300, vmax=500)
    plt.show()
    np.save()

else:
    print("wrong bulet")

