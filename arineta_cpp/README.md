# Deployment files
This file contains instructions for activating the C++ interface that runs the python code, denoising raw data images sent by the machine. Any changes maid in the EDCNN training model are in the .ckpt file, so if changes are made this file is to be changed. If model architecture is also changed, then the model.py file in Python/src folder also should be changed accordingly. 
# API
API details can be found [here](Cpp_API_Proposal.pdf). The class specified in the API can be found in CRSIPProcessor.cpp and CRSIPProcessor.h. 
## Git files
### C++ Code blocks and files- Cpp folder
* **3rdParty**- zmq packages
* **CRSIPProcessor**- Contains function calls and definitions for exchanging messages with child process, and running child process.
* **CRSIPProcessorRunner**- Example code for using the CRSIPProcessor class. Use for running the C++ interface. Inside this folder there is also a zmq 4.2.1 builder, that Visual Studio uses when first building solution. 
* **arr_3d.rawData**- A raw data input file for testing. 
* **CRSIPProcessor.sln**- Solution file. 
### Python Code blocks- Python folder
* **rsip_server.py**- Main python code, that is run by C++ interface.
* **solver.py**- Processing the data and performs denoising (shorter version than in full code).
* **config.py**- Config file containing variable definitions.
* **src/model.py**- Exact same model as in full code.
* **model_230000iter.ckpt**- Pre trained weights of best performing model, to load in test.
## Run the example code
### environment:
Same python environment as in the full code. Make sure environment installed is up to date.

### instructions:
The input is a rawData file representing a (hight,width,num_slices) array containing the CT data. The output is saved also at the same structure, in CRSIPProcessorRunner folder. To activate follow the following instructions:

* Open the CRSIPProcessor.sln with Visual Studio.
* Build solution
* Set "CRSIPProcessorRunner" as Startup project
* Add the command arguments as explained below to "CRSIPProcessorRunner" (right click "CRSIPProcessorRunner"-> properties-> Debugging-> command argument edit).
* Run the code.
  
#### command argument
The command argument contains the inputs values to the interface. The structure of the command is 7 strings:
1) Path to input raw data.
2) Image height
3) Image width
4) Number of slices.
5) Path to python executable (in the relevant conda environment, python.exe file).
6) Path to python file to activate


**Example:** C:\Users\omerc\Documents\arineta_final\arineta_cpp\Cpp\arr_3d.rawData 512 512 280 C:\Users\omerc\anaconda3\envs\conda-supersonic\python.exe C:\Users\omerc\Documents\arineta_final\arineta_cpp\Python\rsip_server.py
