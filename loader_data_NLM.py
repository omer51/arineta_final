import os
import numpy as np
from torch.utils.data import Dataset, DataLoader
from glob import glob as glob
import scipy
import scipy.ndimage
import config


class ct_dataset(Dataset):
    def __init__(self, mode, saved_path, test_patient, patch_n=None, patch_size=None, noise=False):
        """
        creates dataset from numpy files
        :param mode: str, 'train' or 'test'
        :param saved_path: str, path to numpy type images on which to train or test
        :param test_patient: str, name of test case patient, to avoid using in training
        :param patch_n: int, number of patches to crop
        :param patch_size: int, height and width of each patch
        :param noise: bool, if True, randomly noising input training data
        """
        assert mode in ['train', 'test'], "mode is 'train' or 'test'"

        input_path = sorted(glob(os.path.join(saved_path, '*_original.npy')))
        if config.double_input:
            input2_path = sorted(glob(os.path.join(saved_path, '*_SMOOTH.npy')))
        target_path = sorted(glob(os.path.join(saved_path, '*_ASIR.npy')))
        target_smoothed_path = sorted(glob(os.path.join(saved_path, '*_NLM.npy')))
        self.patch_n = patch_n
        self.patch_size = patch_size
        self.noise = noise
        self.mode = mode

        if mode == 'train':
            self.input_ = [f for f in input_path if test_patient not in f]
            if config.double_input:
                self.input2_ = [f for f in input2_path if test_patient not in f]
            self.target_ = [f for f in target_path if test_patient not in f]
            self.target_smoothed_ = [f for f in target_smoothed_path if test_patient not in f]

        else:  # mode =='test'
            self.input_ = [f for f in input_path]
            if config.double_input:
                self.input2_ = [f for f in input2_path]

    def __len__(self):
        return len(self.input_)

    def __getitem__(self, idx):
        if self.mode == 'train':
            input_img, target_img, target_smoothed = self.input_[idx], self.target_[idx], self.target_smoothed_[idx]
            input_img, target_img, target_smoothed_img = np.load(input_img), np.load(target_img), np.load(
                target_smoothed)
            if config.double_input:
                input_img2 = self.input2_[idx]
                input_img2 = np.load(input_img2)

            # divide to patches
            if self.patch_size:
                if config.double_input:
                    input_patches, input_patches2, target_patches, target_smoothed_patches = get_patch(input_img,
                                                                                                       target_img,
                                                                                                       target_smoothed_img,
                                                                                                       self.patch_n,
                                                                                                       self.patch_size,
                                                                                                       input_img2)
                else:
                    input_patches, target_patches, target_smoothed_patches = get_patch(input_img, target_img,
                                                                                   target_smoothed_img,
                                                                                   self.patch_n, self.patch_size)

                # noise training
                if self.noise:
                    gaus = np.random.normal(0, 0.05, input_patches.shape)
                    input_patches = input_patches + gaus * input_patches

                if config.double_input:
                    return input_patches, input_patches2, target_patches, target_smoothed_patches
                else:
                    return input_patches, target_patches, target_smoothed_patches
            else:
                if config.double_input:
                    return input_img, input_img2, target_img, target_smoothed_img
                else:
                    return input_img, target_img, target_smoothed_img
        else:  # mode is test
            if config.double_input:
                input_img, input_img2 = np.load(self.input_[idx]), np.load(self.input2_[idx])
                return input_img, input_img2
            else:
                input_img = np.load(self.input_[idx])
                return input_img


def denormalize_(image):
    image = image * (3072.0 - -1024.0) + -1024.0
    return image


def center_crop(im, im_shape):
    """
    crop center of image when zoom augmentation is used
    :param im: (np array[H,W,B]), images to center crop shaped [h,w,B]
    :param im_shape: tupple, shape of output image (h, w)
    :return: cropped image
    """
    h_center, w_center = int(im.shape[0] / 2), int(im.shape[1] / 2)
    h_start, h_end = int(h_center - im_shape[0] / 2), int(h_center + im_shape[0] / 2)
    w_start, w_end = int(w_center - im_shape[1] / 2), int(w_center + im_shape[1] / 2)
    return im[h_start:h_end, w_start:w_end, :]


def get_patch(full_input_img, full_target_img, full_target_smoothed, patch_n, patch_size, full_input_img2 = None, rescale=False, rotate=False, flip=False):
    """
    preparation of random patches of training data and augmentations if necessary
    :param full_input_img: (numpy array[B,1,H,W]), input image
    :param full_target_img: (numpy array[B,1,H,W]), ASIR image
    :param full_target_smoothed: (numpy array[B,1,H,W]), ASIR+NLM image
    :param patch_n: int, number of patches to crop
    :param patch_size: int, height and width of each patch
    :param full_input_img2: (numpy array[B,1,H,W]), second input image if double input is used
    :param rescale: Bool, if True, zoom in randomly
    :param rotate: Bool, if True, rotate randomly
    :param flip: Bool, if True, flip horizontally randomly
    :return: input image patches, ASIR image patches, ASIR+NLM image patches,
             each of size [B,patch_n,patch_size,patch_size]
    """
    assert full_input_img.shape == full_target_img.shape
    patch_input_imgs = []
    patch_target_imgs = []
    patch_target_smoothed_imgs = []
    h, w = full_input_img.shape
    new_h, new_w = patch_size, patch_size
    if config.double_input:
        patch_input_imgs2 = []
        arr = np.stack([full_input_img, full_target_img, full_target_smoothed, full_input_img2], axis=-1)
    else:
        arr = np.stack([full_input_img, full_target_img, full_target_smoothed], axis=-1)

    # rotation augmentation
    if rotate:
        ang = np.random.randint(-45, 45)
        arr = scipy.ndimage.rotate(arr, ang, reshape=False)

    if not config.double_input:
        # zoom augmentation - works only on single input
        if rescale:
            scale = np.random.uniform(1, 2)
            # print(arr.shape, scale)
            zoom_arr = scipy.ndimage.zoom(arr, (scale, scale, 1))
            arr = center_crop(zoom_arr, arr.shape)
            # print('array_shape', arr.shape)

    for i in range(patch_n):
        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        patch_input_img = arr[top:top + new_h, left:left + new_w, 0]
        patch_target_img = arr[top:top + new_h, left:left + new_w, 1]
        patch_target_smoothed = arr[top:top + new_h, left:left + new_w, 2]
        if config.double_input:
            patch_input_img2 = arr[top:top + new_h, left:left + new_w, 3]


        # flip horizontal augmentation
        if flip:
            prob = np.random.randint(0,10)
            if prob>5:
                patch_input_img = np.flip(patch_input_img, 1)
                patch_target_img = np.flip(patch_target_img, 1)
                patch_target_smoothed = np.flip(patch_target_smoothed, 1)
                if config.double_input:
                    patch_input_img2 = np.flip(patch_input_img2, 1)

        patch_input_imgs.append(patch_input_img)
        patch_target_imgs.append(patch_target_img)
        patch_target_smoothed_imgs.append(patch_target_smoothed)
        if config.double_input:
            patch_input_imgs2.append(patch_input_img2)
    if config.double_input:
        return np.array(patch_input_imgs), np.array(patch_input_imgs2), \
               np.array(patch_target_imgs), np.array(patch_target_smoothed_imgs)
    else:
        return np.array(patch_input_imgs), np.array(patch_target_imgs), np.array(patch_target_smoothed_imgs)


def get_loader(mode='train', saved_path=None, test_patient='L506',
               patch_n=None, patch_size=None,batch_size=2, num_workers=6):
    """
    get dataloader from dataset
    :param mode: str, train or test
    :param saved_path: str, path to numpy type images on which to train or test
    :param test_patient: str, name of test case patient, to avoid using in training
    :param patch_n: int, number of patches to crop
    :param patch_size: int, height and width of each patch
    :param batch_size: int, number of cases in mini batch
    :param num_workers: int, number of parallel processors
    :return: dataloader
    """

    dataset_ = ct_dataset(mode, saved_path, test_patient, patch_n, patch_size)
    should_shuffle = True if mode == 'train' else False
    data_loader = DataLoader(dataset=dataset_, batch_size=batch_size, shuffle=should_shuffle,
                             num_workers=num_workers, worker_init_fn=np.random.seed)
    return data_loader


