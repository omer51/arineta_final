import os
import argparse
from torch.backends import cudnn
from loader_data_NLM import get_loader
import config
import time
from pathlib import Path
import glob
from solver import Solver
from prep import get_ct_path_from_dataset_NLM, save_dataset_z

def main(args):
    cudnn.benchmark = True

    dataset_path = Path(args.data_path)

    # if needed, auto generate outputs path
    if args.generate_path:
        main_dir, folder_name = os.path.split(Path(dataset_path))
        args.np_save_path = os.path.join(main_dir, folder_name+"_outputs", folder_name+"_npy")
        args.saved_path = args.np_save_path
        args.save_path = os.path.join(main_dir, folder_name+"_outputs", folder_name+"_prediction")
        if args.single_case:
            args.dicom_path = Path(glob.glob(os.path.join(dataset_path, args.single_case,"*"))[0])
        else:
            args.dicom_path = glob.glob(os.path.join(dataset_path,"*","*"))[0]

    # prep data if needed
    if args.auto_prep:
        print(args.data_path)
        # prepare paths dict
        data_set = get_ct_path_from_dataset_NLM(dataset_path, args.single_case)
        # generate and save numpy copy of data
        save_dataset_z(args, data_set)

    # make save dir
    t_start = time.time()
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path)
        print('Create path : {}'.format(args.save_path))
    if args.mode == 'test':
        npy_path = os.path.join(args.save_path, 'npy', str(args.test_iters), args.test_patient)
        if not os.path.exists(npy_path):
            os.makedirs(npy_path)
            print('Create path : {}'.format(npy_path))

    # create dataset loader
    data_loader = get_loader(mode=args.mode,
                             saved_path=args.saved_path,
                             test_patient=args.test_patient,
                             patch_n=(config.patch_n if args.mode=='train' else None),
                             patch_size=(config.patch_size if args.mode=='train' else None),
                             batch_size=(config.batch_size if args.mode=='train' else 1),
                             num_workers=args.num_workers)
    print('number of mini batches:', len(data_loader))
    print('number of types for each case:', len(next(iter(data_loader))))
    solver = Solver(args, data_loader)
    if args.mode == 'train':
        if config.double_input:
            solver.train_double()
        else:
            solver.train()
    elif args.mode == 'test':
        if config.double_input:
            solver.test_double()
        else:
            solver.test()
    print("testing took {} seconds".format(int(time.time()-t_start)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--generate_path', type=bool, default=config.generate_path)
    parser.add_argument('--mode', type=str, default=config.mode)
    parser.add_argument('--saved_path', type=str, default=config.saved_path)
    parser.add_argument('--save_path', type=str, default=config.save_path)
    parser.add_argument('--transfer_weights', type=str, default=config.transfer_weights)
    parser.add_argument('--test_iters', type=int, default=config.test_iters)
    parser.add_argument('--test_patient', type=str, default=config.test_patient)
    parser.add_argument('--dicom_path', type=str,default=config.dicom_path)
    parser.add_argument('--series_description', type=str, default=config.series_description)
    parser.add_argument('--apply_nlm', type=bool, default=config.apply_nlm)

    # prep parameters
    parser.add_argument('--auto_prep', type=str, default=config.auto_prep)
    parser.add_argument('--data_path', type=str, default=config.input_data_path)
    parser.add_argument('--np_save_path', type=str, default=config.np_save_path)
    parser.add_argument('--single_case', type=str, default=config.single_case)
    parser.add_argument('--create_NLM', type=str, default=config.create_nlm)

    # training parameters
    parser.add_argument('--init_with_transfer', type=bool, default=config.init_with_transfer)

    # gpu definitions
    parser.add_argument('--device', type=str, default=config.device)
    parser.add_argument('--num_workers', type=int, default=config.num_workers)

    args = parser.parse_args()
    t_0 = time.time()
    main(args)
    print('process took {} seconds'.format(int(time.time()-t_0)))

