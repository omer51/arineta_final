import torch
import torch.nn as nn
from torch.nn.modules.loss import _Loss
from torchvision import models
import matplotlib.pyplot as plt

class ResNet50FeatureExtractor(nn.Module):

    def __init__(self, blocks=[1, 2, 3, 4], pretrained=False, progress=True, **kwargs):
        """
        main model for perceptual loss
        :param blocks(list of int in range 1-4): determines which resnet layers to compare
        :param pretrained(bool): If True, returns a model pre-trained on ResNet
        :param progress(bool): If True, displays a progress bar of the download to stderr
        """
        super(ResNet50FeatureExtractor, self).__init__()
        self.model = models.resnet50(**kwargs)
        del self.model.avgpool
        del self.model.fc
        self.blocks = blocks

    def forward(self, x):
        """
        :param x(Tensor[B,3,H,W]): input tensor
        :return: list of outputs of chosen layers
        """
        feats = list()

        x = self.model.conv1(x)
        x = self.model.bn1(x)
        x = self.model.relu(x)
        x = self.model.maxpool(x)

        x = self.model.layer1(x)
        if 1 in self.blocks:
            feats.append(x)

        x = self.model.layer2(x)
        if 2 in self.blocks:
            feats.append(x)

        x = self.model.layer3(x)
        if 3 in self.blocks:
            feats.append(x)

        x = self.model.layer4(x)
        if 4 in self.blocks:
            feats.append(x)

        return feats


class CompoundLoss(_Loss):

    def __init__(self, blocks=[1, 2, 3, 4], mse_weight=1, resnet_weight=0.01):
        """
        main model for loss calculations. combines mse and perceptual loss.
        :param blocks(list of int in range 1-4): determines which resnet layers to compare
        :param mse_weight(float): weight of MSE loss
        :param resnet_weight(float): weight of perceptual loss
        """
        super(CompoundLoss, self).__init__()

        self.mse_weight = mse_weight
        self.resnet_weight = resnet_weight

        self.blocks = blocks
        self.blocks2 = blocks
        self.model = ResNet50FeatureExtractor(pretrained=True)
        self.model2 = ResNet50FeatureExtractor(pretrained=True)
        if torch.cuda.is_available():
            self.model = self.model.cuda()
            self.model2 = self.model2.cuda()
        self.model.eval()
        self.model2.eval()

        self.criterion = nn.MSELoss()


    def forward(self, pred, asir_nlm, target):
        """
        :param pred: (Tensor[B,1,H,W]) ED-CNN prediction
        :param asir_nlm: (Tensor[B,1,H,W]) smooth ground truth, ASIR+NLM
        :param target: (Tensor[B,1,H,W]) sharp features ground truth, ASIR
        :return: training loss
        """
        loss_value = 0

        # perceptual loss
        pred_feats = self.model(torch.cat([pred, pred, pred], dim=1))
        target_fetaures = self.model(torch.cat([target, target, target], dim=1))
        feats_num = len(self.blocks)
        for idx in range(feats_num):
            loss_value += self.criterion(pred_feats[idx], target_fetaures[idx])
        loss_value /= feats_num

        """
        target_fetaures2 = self.model(torch.cat([target, target, target], dim=1))
        loss_value2 = 0
        feats_num2 = len(self.blocks2)
        for idx in range(feats_num2):
            loss_value2 += self.criterion(pred_feats[idx], target_fetaures2[idx])
        loss_value2 /= feats_num2
        """
        # total loss
        loss = self.mse_weight * self.criterion(pred, asir_nlm) + self.resnet_weight * loss_value #+ self.resnet_weight * loss_value2

        return loss
